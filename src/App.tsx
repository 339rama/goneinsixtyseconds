import React from 'react';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@emotion/react';
import { theme } from './styles/theme/theme';
import store from './store/store';
import CarsScreenListScreen from './modules/cars/ui/screens/list';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <StatusBar backgroundColor="#222222" />
        <CarsScreenListScreen />
      </Provider>
    </ThemeProvider>
  );
};

export default App;
