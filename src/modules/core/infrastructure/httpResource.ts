import { FetchResource } from '@snap-alex/domain-js';
import fetch from 'cross-fetch';
import Config from 'react-native-config';

const httpResource = new FetchResource(
  Config.API_URL,
  {
    headers: {
      'Content-Type': 'application/json',
    },
    trailingSlash: true,
    timeOffset: false,
  },
  fetch,
);

export default httpResource;
