import { createSlice } from '@reduxjs/toolkit';
import { loadCars, loadNextCars } from './actions';
import carsEntitiesAdapter from './adapters';

const slice = createSlice({
  name: 'cars',
  initialState: carsEntitiesAdapter.getInitialState({
    isLoading: false,
    page: 1,
  }),
  reducers: {},
  extraReducers: builder => {
    builder.addCase(loadCars.pending, state => {
      state.isLoading = true;
    });
    builder.addCase(loadCars.fulfilled, (state, { payload }) => {
      state.isLoading = false;
      carsEntitiesAdapter.addMany(state, payload);
    });
    builder.addCase(loadCars.rejected, state => {
      state.isLoading = false;
    });
    builder.addCase(loadNextCars.pending, state => {
      state.isLoading = true;
    });
    builder.addCase(loadNextCars.fulfilled, (state, { payload }) => {
      state.isLoading = false;
      state.page = payload.page;
      carsEntitiesAdapter.addMany(state, payload.items);
    });
    builder.addCase(loadNextCars.rejected, state => {
      state.isLoading = false;
    });
  },
});

const carsReducer = slice.reducer;
export type CarsStore = ReturnType<typeof carsReducer>;

export default carsReducer;
