export interface Car {
  id: number;
  carId: string;
  until: string;
  price: number;
  locations: string[];
  color: string;
  destination: string;
  photo: string;
  make: string;
  model: string;
  year: string;
}
