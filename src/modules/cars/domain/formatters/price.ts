export function formatPrice(price: string | number) {
  return price
    .toString()
    .replace(/^0+/g, '')
    .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}
