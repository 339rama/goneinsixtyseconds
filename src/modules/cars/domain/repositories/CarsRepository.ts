import { BaseRepository } from '@snap-alex/domain-js';
import { Car } from '../interfaces/Car';
import carsResource from '../resources/CarsResource';

export class CarsRepository extends BaseRepository<any> {
  public loadCars(): Promise<Car[]> {
    return this.resource().get();
  }
}

const carsRepository = new CarsRepository(carsResource);
export default carsRepository;
