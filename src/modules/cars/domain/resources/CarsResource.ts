import { BaseRestResource } from '@snap-alex/domain-js';
import httpResource from '@src/modules/core/infrastructure/httpResource';

class CarsResource extends BaseRestResource {}

const carsResource = new CarsResource(
  httpResource,
  '37e5b208-6b51-4a2d-a59b-af126c7a09c9',
);
export default carsResource;
