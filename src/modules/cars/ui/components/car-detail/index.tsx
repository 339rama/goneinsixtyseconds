import unifiedDate from '@src/modules/ud-ui/unified-date';
import React from 'react';
import { Car } from '../../../domain/interfaces/Car';
import CarsComponentsInfoField from '../common/info-field';
import CarsComponentsCarPlate from '../car-plate';
import Locations from './locations';
import * as S from './styles';
import { formatPrice } from '@src/modules/cars/domain/formatters/price';

type Props = {
  car: Car;
};

const CarsComponentsCarDetail = (props: Props) => {
  const { car } = props;
  return (
    <S.DetailContainer>
      <S.DetailImage source={{ uri: car.photo }}></S.DetailImage>

      <CarsComponentsCarPlate
        mode="dark"
        value={car.carId}
        style={{ textAlign: 'center' }}
      />

      <S.InfoContainer>
        <CarsComponentsInfoField description="Make" value={car.make} />
        <CarsComponentsInfoField description="Model" value={car.model} />
        <CarsComponentsInfoField description="Year" value={car.year} />
        <CarsComponentsInfoField
          description="Purchase"
          value={`$${formatPrice(car.price)}`}
        />
        <CarsComponentsInfoField description="Dest" value={car.destination} />
        <CarsComponentsInfoField
          description="Until"
          value={unifiedDate(new Date(car.until))}
        />
        <Locations locations={car.locations} />
      </S.InfoContainer>
      <S.ButtonContainer>
        <S.ButtonText>Hijact</S.ButtonText>
      </S.ButtonContainer>
    </S.DetailContainer>
  );
};

export default CarsComponentsCarDetail;
