import React, { useCallback } from 'react';
import { Car } from '@src/modules/cars/domain/interfaces/Car';
import * as S from './styles';
import { View } from 'react-native';
import CarsComponentsInfoField from '../../common/info-field';
import CarsComponentsCarPlate from '../../car-plate';
import unifiedDate from '@src/modules/ud-ui/unified-date';
import { formatPrice } from '@src/modules/cars/domain/formatters/price';

type Props = {
  car: Car;
  showDetail: (car: Car) => any;
};

const CarsComponentsListItem = (props: Props) => {
  const { car, showDetail } = props;
  const onMorePressed = useCallback(() => {
    showDetail(car);
  }, [car]);
  return (
    <S.CarListCard onPress={onMorePressed}>
      <View>
        <S.CarPhoto source={{ uri: car.photo }}></S.CarPhoto>
        <CarsComponentsCarPlate value={car.carId} mode="light" />
      </View>
      <S.InfoContainer>
        <CarsComponentsInfoField description="Make" value={car.make} />
        <CarsComponentsInfoField description="Model" value={car.model} />
        <CarsComponentsInfoField description="Year" value={car.year} />
        <CarsComponentsInfoField
          description="Purchase"
          value={`$${formatPrice(car.price)}`}
        />
        <CarsComponentsInfoField description="Dest" value={car.destination} />
        <CarsComponentsInfoField
          description="Until"
          value={unifiedDate(new Date(car.until))}
        />
      </S.InfoContainer>
    </S.CarListCard>
  );
};

export default React.memo(CarsComponentsListItem);
