import styled from '@emotion/native';

type Props = {
  mode: 'dark' | 'light';
};

export const Plate = styled.Text<Props>`
  border-radius: 4px;
  border: 2px solid ${({ mode }) => (mode === 'dark' ? 'white' : 'black')};
  padding: 5px;
  background-color: ${({ mode }) => (mode === 'dark' ? 'black' : 'white')};
  font-size: 20px;
  letter-spacing: 5px;
  color: ${({ mode }) => (mode === 'dark' ? 'white' : 'black')};
  text-transform: uppercase;
  font-family: NewsCycle-Bold;
`;
