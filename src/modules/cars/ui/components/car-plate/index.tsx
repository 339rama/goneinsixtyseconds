import React from 'react';
import { TextProps } from 'react-native';
import { Plate } from './styles';

type Props = {
  value: string;
  mode: 'light' | 'dark';
} & Omit<TextProps, 'children'>;

const CarsComponentsCarPlate = (props: Props) => {
  const { mode, value, ...textProps } = props;
  return (
    <Plate mode={mode} {...textProps}>
      {value}
    </Plate>
  );
};

export default CarsComponentsCarPlate;
