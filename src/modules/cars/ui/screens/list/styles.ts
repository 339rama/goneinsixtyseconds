import styled from '@emotion/native';

export const Container = styled.SafeAreaView`
  background-color: #4f4f4e;
  height: 100%;
`;

export const ContentContainer = styled.View`
  padding-horizontal: 24px;
  height: 100%;
`;

export const HeaderContainer = styled.View`
  background-color: white;
  padding: 20px;
`;

export const Header = styled.Text`
  font-size: 24px;
  font-family: Monoton-Regular;
  color: #000;
`;
