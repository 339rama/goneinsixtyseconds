import React from 'react';
import Animated from 'react-native-reanimated';
import { StyleSheet } from 'react-native';
import { Modalize, ModalizeProps } from 'react-native-modalize';
import { RefObject } from 'react';
import * as S from './styles';

export type UDBottomSheetProps = {
  sheetRef: RefObject<Modalize>;
  children: JSX.Element;
  onCloseEnd?: (...args: any) => any;
} & Omit<ModalizeProps, 'ref' | 'HeaderComponent' | 'children'>;

const UDBottomSheetModalize = (props: UDBottomSheetProps) => {
  const { children, onCloseEnd, sheetRef, ...modalizeProps } = props;
  const fall = new Animated.Value(1);
  const animatedShadowOpacity = Animated.interpolateNode(fall, {
    inputRange: [0, 1],
    outputRange: [0.5, 0],
  });
  return (
    <>
      <S.AnimatedShadowOpacity
        pointerEvents="none"
        style={[
          {
            ...StyleSheet.absoluteFillObject,
            opacity: animatedShadowOpacity,
          },
        ]}
      />
      <Modalize
        ref={sheetRef}
        onClose={onCloseEnd}
        handlePosition="inside"
        modalStyle={{
          borderTopLeftRadius: 40,
          borderTopRightRadius: 40,
          overflow: 'hidden',
          // backgroundColor: 'transparent',
        }}
        {...modalizeProps}>
        {children}
      </Modalize>
    </>
  );
};

export default UDBottomSheetModalize;
