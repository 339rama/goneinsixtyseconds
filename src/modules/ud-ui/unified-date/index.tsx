import React from 'react';

const prependWithZero = (value: number): string => {
  return value < 10 ? `0${value}` : `${value}`;
};

const unifiedDate = (value: Date) => {
  const date = prependWithZero(value.getDate());
  const month = prependWithZero(value.getMonth());
  const year = value.getFullYear();
  return `${date}-${month}-${year}`;
};

export default unifiedDate;
