import React, { RefObject, useCallback, useRef } from 'react';
import { Modalize } from 'react-native-modalize';

export const useBottomSheet = () => {
  const ref = useRef(null) as RefObject<Modalize>;
  const open = useCallback(() => {
    ref.current?.open();
  }, [ref]);
  const close = useCallback(() => {
    ref.current?.close();
  }, [ref]);
  return { ref, open, close };
};
