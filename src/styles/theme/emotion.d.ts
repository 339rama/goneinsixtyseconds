import '@emotion/react';

export interface ThemeColors {
  dark: string;
  light: string;
}

declare module '@emotion/react' {
  export interface Theme {
    colors: ThemeColors;
    fonts: {
      monoton: string;
      newsCycle: string;
      delaGothic: string;
    };
  }
}
