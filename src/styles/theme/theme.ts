import { Theme } from '@emotion/react';

export const theme: Theme = {
  colors: {
    dark: '#000',
    light: '#FFF',
  },
  fonts: {
    monoton: 'Monoton-Regular',
    newsCycle: 'NewsCycle-Regular',
    delaGothic: 'DelaGothicOne-Regular',
  },
};
