import carsReducer from '../modules/cars/store';

export default {
  cars: carsReducer,
};
