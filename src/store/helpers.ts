import { Dispatch } from '@reduxjs/toolkit';

export const dispatchAction = (
  dispatch: Dispatch<any>,
  action: any,
): ((...args: any) => any) => {
  return function (...args: any) {
    dispatch(action(...args));
  };
};
